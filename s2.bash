#!/bin/bash
read -p "Enter your Last Name: " last_name
read -p "Enter your First Name: " first_name
read -p "Enter your birthdate (mm-dd-yyyy): " birthdate

birthmonth=${birthdate:0:2}
birthday=${birthdate:3:2}
birthyear=${birthdate:6:4}

cdate=`date +%m-%d-%Y`

c_month=${cdate:0:2}
c_day=${cdate:3:2}
c_year=${cdate:6:4}

if [[ "$c_month" -lt "$((10#$birthmonth))" ]] || [[ "$c_month" -eq "$birthmonth" && "$c_day" -lt "$birthday" ]]
then
  let age=c_year-birthyear-1
else
  let age=c_year-birthyear
fi
echo "Hello, $first_name $last_name! You're $age years old!"
